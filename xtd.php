<?php 

#XTD:xspilk00

/* --- MAIN ------------------------------------------------------------------*/

error_reporting(0);

mb_internal_encoding("UTF-8");

$params = doParams($argv, $argc);

if(isset($params["--output="])) {
    if(empty($params["--output="]))
        printErr("PARAMS", 1);
    if(($output = fopen($params["--output="], "w")) === false) {
        printErr("OUTPUT_FILE", 3);
    }
}
else {
    $output = fopen("php://stdout", "w");
}

if(array_key_exists("--help", $params))
    printHelp($output);

if(isset($params["--input="])) {
    if(empty($params["--input="]))
        printErr("PARAMS", 1);
    if(($input = fopen($params["--input="], "r")) === false) {
        printErr("INPUT_FILE", 2);
    }
}
else {
    $input = fopen("php://stdin", "r");
}

$input_text = '';
while (false !== ($char = fgetc($input))) {
    $input_text .= $char;
}

if(isset($params["--header="])) {
    fprintf($output, "--%s\n\n", $params["--header="]);
}

$xml = simplexml_load_string($input_text);

if(count($xml) < 1)
    exit(0);

$relations = array();
$relations = RecurseXML($xml, $relations);

$xml_array = xml2array($xml);

$tables = array();

$tables = getTables($xml_array, $tables);

if (!array_key_exists("-g", $params))
    $out = createTables($tables, $relations, $params);
else
    $out = relationsTables($tables, $relations, $params);

fprintf($output, "%s", $out);

fclose($output);
fclose($input);

/* --- FUNCTIONS -------------------------------------------------------------*/

/*
FUNKCE PRO ZPRACOVÁNÍ POLE VYTVOŘENÉHO Z XML 
VYTVAŘÍ POLE TABULEK S JEJICH ATRIBUTY, U NICHŽ DOPLŇUJE DATOVÝ TYP
*/

function getTables($array, $tables, $prev = "") {
    foreach ($array as $key => $value) {
        if(is_string($key) && is_array($value) && $key != "@attributes") {
            $key = mb_strtolower($key);
            if(!array_key_exists($key, $tables)) {
                $tables[$key] = array("atributy" => array(), "keys" => array());
            }
            
            $prev = $key;
        }
        elseif (is_string($key) && is_array($value) && $key == "@attributes") {
            foreach ($value as $key_atr => $atribut) {
                $key_atr = mb_strtolower($key_atr);
                if(isset($tables[$prev]["atributy"][$key_atr]))
                    $tables[$prev]["atributy"][$key_atr] = resultDataType($tables[$prev]["atributy"][$key_atr] ,getDataType($atribut));
                else
                    $tables[$prev]["atributy"][$key_atr] = getDataType($atribut);
            }
        }
        elseif (is_string($key) && $key == "@value") {
            if(isset($tables[$prev]["value"]))
                $tables[$prev]["value"] = resultDataType($tables[$prev]["value"] ,getDataType($value, false));
            else
                $tables[$prev]["value"] = getDataType($value, false);
        }
        
        if(is_array($value))
            $tables = getTables($value, $tables, $prev);
    }
    
    return $tables;
}

/*
FUNKCE PRO ZJIŠTĚNÍ VZTAHŮ OTEC - SYN MEZI TABULKAMI
VÝSTUP ULOŽEN V POLI JAKO $array['otec']['syn']
*/

function RecurseXML($xml, $relations, $parent="")
{
    $arr = array();
    $parent = mb_strtolower($parent);
    foreach ($xml as $key => $person) {
        $key = mb_strtolower($key);
        if(!array_key_exists($key, $relations)) {
            $relations[mb_strtolower($key)] = array();
        }
        if(!empty($parent)) {
            if(array_key_exists($key, $arr)) {
                $arr[$key]++;
            }
            else {
                $arr[$key] = 1;
            }
        }
        $relations = RecurseXML($person, $relations, $key);
    }

    foreach ($arr as $key => $value) {
        $key = mb_strtolower($key);
        if(!empty($parent)) {
            if(array_key_exists($key, $relations[$parent])) {
                if($relations[$parent][$key] < $arr[$key]) {
                    $relations[$parent][$key] = $arr[$key];
                }
            }
            else {
                $relations[$parent][$key] = $arr[$key];
            }
        }
    }

    return $relations;
}

/*
FUNKCE PRO GENEROVÁNÍ VÝSTUPU S OHLEDEM NA ZADANÉ PARAMETRY
TATO FUNKCE NENÍ VYUŽITA PŘI ZADÁNÍ PARAMETRU -g
*/

function createTables($tables, $relations, $params) {
    $out = '';

    foreach ($tables as $key => $value) {
        $i = $j = 0;
        $out .= sprintf("CREATE TABLE %s (\n\tprk_%s_id INT PRIMARY KEY", $key, $key);
        if(array_key_exists("prk_".$key."_id", $value["atributy"]))
            printErr("KEY_CONFLICT", 90);

        $value["keys"]["prk_".$key."_id"] = "INT";
        foreach ($relations[$key] as $key_atr => $atribut) {
            if((!array_key_exists("--etc=", $params) && !array_key_exists("-b", $params)) || ($params["--etc="] >= $atribut)) {
                for($a = 1; $a <= $atribut; $a++) {
                    $i++;
                    $atr_name = $key_atr . ($atribut > 1 ? strval($a) : "") . "_id";
                    if(array_key_exists($atr_name, $value["atributy"]) || array_key_exists($atr_name, $value["keys"]))
                        printErr("KEY_CONFLICT", 90);

                    $value["keys"][$atr_name] = "INT";

                    $out .= sprintf(",\n\t%s %s", $atr_name, "INT");
                }
            }
            if(array_key_exists("-b", $params)) {
                $key_atr .= "_id";
                if(array_key_exists($key_atr, $value["atributy"]) || array_key_exists($key_atr, $value["keys"]))
                        printErr("KEY_CONFLICT", 90);

                $value["keys"][$atr_name] = "INT";

                $out .= sprintf(",\n\t%s %s", $key_atr, "INT");
            }
        }

        foreach ($relations as $parent => $child) {
            $parent_id = $parent . "_id";
            foreach ($relations[$parent] as $key_child => $atribut) {
                if(array_key_exists("--etc=", $params) && $params["--etc="] < $atribut && $key_child == $key) {
                    $i++;
                    if(array_key_exists($parent_id, $value["atributy"]) || array_key_exists($parent_id, $value["keys"]))
                        printErr("KEY_CONFLICT", 90);

                    $out .= sprintf(",\n\t%s %s", $parent_id, "INT");
                }
            }               
        }

        if(array_key_exists("-a", $params)) {
            if(isset($value["value"])) $out .= sprintf(",\n\tvalue %s", $value["value"]);
        }
        else {
            foreach ($value["atributy"] as $key_atr => $atribut) {
                $i++;
                if($key_atr != "value")
                    $out .= sprintf(",\n\t%s %s", $key_atr, $atribut);
                else
                    $value["value"] = resultDataType($value["value"], $atribut);
            }

            if(isset($value["value"])) $out .= sprintf(",\n\tvalue %s", $value["value"]);
        }
        $out .= sprintf("\n);\n\n");
    }

    return $out;
}

/*
FUNKCE KTERÁ JE VOLÁNA PŘI ZADÁNÍ PARAMETRU -g
ROZPOZNÁVÁ VZTAHY MEZI JEDNOTLIVÝMY TABULKAMI
*/

function relationsTables(&$tables, &$relations, $params) {
    $relationship_arr = getRelationshipArray($tables, $relations, $params);

    foreach ($tables as $key => $value) {
        $relationship_arr[$key][$key] = getRelationship($relationship_arr, $key, $key);
    }

    foreach ($relationship_arr as $key => $value) {
        foreach ($relationship_arr[$key] as $key_child => $value_child) {
            $relationship_arr[$key][$key_child] = getRelationship($relationship_arr, $key, $key_child);
        }
    }

    foreach ($relationship_arr as $a => $value) {
        foreach ($relationship_arr[$a] as $b => $value_child) {
            if($relationship_arr[$a][$b] != "N:M") {
                $rel = explode(":", $relationship_arr[$a][$b]);
                $relationship_arr[$b][$a] = $rel[1] . ":" . $rel[0];
            }
            else {
                $relationship_arr[$b][$a] = "N:M";
            }

        }
    }

    $change = true;
    while($change) {
        $change = false;
        foreach ($relationship_arr as $a => $value) {
            foreach ($relationship_arr[$a] as $c => $value_child) {
                if($relationship_arr[$a][$c] == "1:N" || $relationship_arr[$a][$c] == "N:1") {
                    $rel_a_c = $relationship_arr[$a][$c];
                    foreach ($relationship_arr[$c] as $b => $value) {
                        if($relationship_arr[$c][$b] == $rel_a_c) {
                            if(!isset($relationship_arr[$a][$b])) {
                                $relationship_arr[$a][$b] = $relationship_arr[$c][$b];
                                $change = true;
                            }
                        }
                    }
                }
            }
        }

    }

    $change = true;
    while($change) {
        $change = false;
        foreach ($relationship_arr as $a => $value) {
            foreach ($relationship_arr[$a] as $c => $value_child) {
                foreach ($relationship_arr[$c] as $b => $value) {
                    if(!isset($relationship_arr[$a][$b])) {
                        $relationship_arr[$a][$b] = $relationship_arr[$b][$a] = "N:M";
                        $change = true;
                    }
                }
            }
        }
    }

    return relationsTableOut($relationship_arr);
}

/*
FUNKCE GENERUJÍCÍ VÝSTUP PŘI ZADANÉM PARAMETRU -g
*/

function relationsTableOut(&$relationship_arr) {
    $out = "<tables>\n";
    foreach ($relationship_arr as $key => $value) {
        $out .= "\t<table name=\"$key\">\n";
        foreach ($value as $key_child => $rel) {
            $out .= "\t\t<relation to=\"$key_child\" relation_type=\"$rel\" />\n";
        }
        $out .= "\t\t</table>\n";
    }
    $out .= "</tables>\n";
    return $out;
}

/*
FUNKCE GENERUJÍCÍ TABULKU VZTAHŮ
*/

function getRelationshipArray(&$tables, &$relations, $params) {
    $relationship_arr = array();
    foreach ($tables as $key => $value) {

        foreach ($relations[$key] as $key_atr => $atribut) {
            if((!array_key_exists("--etc=", $params) && !array_key_exists("-b", $params)) || ($params["--etc="] >= $atribut)) {

                    $relationship_arr[$key][$key_atr] = 1;
            }
            if(array_key_exists("-b", $params)) {
                $relationship_arr[$key][$key_atr] = 1;
            }
        }

        foreach ($relations as $parent => $child) {
            foreach ($relations[$parent] as $key_child => $atribut) {
                if(array_key_exists("--etc=", $params) && $params["--etc="] < $atribut && $key_child == $key) {

                    $relationship_arr[$key][$parent] = 1;
                }
            }               
        }
    }

    return $relationship_arr;
}

/*
FUNKCE ROZPOZNÁVAJÍCÍ VZTAH MEZI 2 SOUSEDNÍMI TABULKAMI
*/

function getRelationship(&$relationship_arr, $item_a, $item_b) {
    if($item_a == $item_b) {
        return "1:1";
    }
    elseif(isset($relationship_arr[$item_a][$item_b]) && isset($relationship_arr[$item_b][$item_a])) {
        return "N:M";
    }
    elseif(isset($relationship_arr[$item_a][$item_b]) && !isset($relationship_arr[$item_b][$item_a])) {
        return "N:1";
    }
    elseif(!isset($relationship_arr[$item_a][$item_b]) && isset($relationship_arr[$item_b][$item_a])) {
        return "1:N";
    }

    return "";

}

/*
FUNKCE ROZPOZNÁVAJÍCÍ VYŠŠÍ ZÚČASTNĚNÝ TYP
*/

function resultDataType($prev, $new) {
    $options = array("BIT" => 0, "INT" => 1, "FLOAT" => 2, "NVARCHAR" => 3, "NTEXT" => 4);
    if($options[$new] > $options[$prev])
        return array_search($options[$new], $options);
    else
        return array_search($options[$prev], $options);
}

/*
FUNKCE NA PŘEVOD VÝSTUPU SIMPLEXML NA ASOCIATIVNÍ POLE
*/

function xml2array($xmlObject)
{
    foreach($xmlObject->attributes() as $attr => $val)
        $out['@attributes'][$attr] = (string)$val;

    $has_childs = false;
    foreach($xmlObject as $index => $node)
    {
        $has_childs = true;
        $out[$index][] = xml2array($node);
    }

    if ((!$has_childs && $val = (string)$xmlObject) || (!$has_childs && (string)$xmlObject == "0" ))
        $out['@value'] = (string)$xmlObject;

    return $out;
}

/*
FUNKCE ROZPOZNÁVAJÍCÍ SKUTEČNÝ DATOVÝ TYP ŘETĚZCE
*/

function getDataType($string, $atribut = true){
    $string = trim($string);
    if(empty($string)) return "BIT";
    if(mb_strtolower($string) == "true" || mb_strtolower($string) == "false" || $string === "0" || $string === "1") return "BIT";
    if(preg_match("/^[-+]?[0-9]+$/", $string)) return "INT";
    if(preg_match("/^[-+]?[0-9]*\.?[0-9]*([eE][-+]?[0-9]+)?$/",$string)) return "FLOAT";
    if($atribut) return "NVARCHAR";
    return "NTEXT";
}

/*
FUNKCE PRO ZPRACOVÁNÍ PARAMETRŮ
*/

function doParams($argv, $argc) {
    $allowed = array("-a", "-b", "-g");
    $options = array();
    
    /* OŠETŘENÍ NÁPOVĚDY */
    if(in_array("--help", $argv)) {
        if($argc > 2) {
            printErr("PARAMS", 1);
        }
        else {
            $options["--help"] = NULL;
        }
    }
    
    // ZAHOZENÍ NÁZVU SKRIPTU
    unset($argv[0]);
    
    // SAMOTNÉ OŠETŘENÍ PARAMETRŮ
    foreach ($argv as $value) {
        if(array_key_exists("--help", $options))
            break;

        $error = 1;
        if(!in_array($value, $allowed)) {
            $substr = '';
            if(substr_compare($value, "--input=", 0, 8) == 0) {
                $substr = substr($value, 8);
                $value = "--input=";
                $error = 0;
            }
            elseif(substr_compare($value, "--output=", 0, 9) == 0) {
                $substr = substr($value, 9);
                $value = "--output=";
                $error = 0;
            }
            elseif(substr_compare($value, "--etc=", 0, 6) == 0) {
                $substr = substr($value, 6);
                $value = "--etc=";
                $error = 0;
            }
            elseif(substr_compare($value, "--header=", 0, 9) == 0) {
                $substr = substr($value, 9);
                $value = "--header=";
                $error = 0;
            }
            
            // OŠETŘENÍ 2 STEJNÝCH PARAMETRŮ
            if(!array_key_exists($value, $options)) {
                $options[$value] = $substr;
            }
            else {
                $error = 1;
            }
        }
        else {
            // OŠETŘENÍ 2 STEJNÝCH PARAMETRŮ
            if(!array_key_exists($value, $options)) {
                $error = 0;
                $options[$value] = NULL;
            }
        }
        
        if($error == 1) {
            printErr("PARAMS", 1);
        }
    }
    
    // CHYBA POKUD JSOU NASTAVENY ZÁROVEŇ -b a --etc
    if(array_key_exists("-b", $options) && array_key_exists("--etc=", $options)) {
        printErr("PARAMS", 1);
    }
    
    // OŠETŘENÍ PARAMETRU ETC (MUSÍ BÝT ČÍSLO)
    if(array_key_exists("--etc=", $options)) {
        if(ctype_digit($options["--etc="])) {
            if(intval($options["--etc="]) < 0) {
                printErr("PARAMS", 1);
            }
        }
        else {
            printErr("PARAMS", 1);
        } 
    }

    return $options;
}

/*
FUNKCE PROVÁDĚJÍCÍ VÝPIS CHYBOVÉ HLÁŠKY A UKONČENÍ SKRIPTU S PŘÍSLUŠNÝM CHYBOVÝM KÓDEM  
*/

function printErr($err, $code) {
    switch ($err) {
        case "PARAMS":
            fprintf(STDERR, "Špatný formát parametrů skriptu nebo byla použita zakázaná kombinace parametrů.\n");
            break;
        case "INPUT_FILE":
            fprintf(STDERR, "Neexistující zadaný vstupní soubor nebo chyba otevření zadaného vstupního souboru pro čtení.\n");
            break;
        case "OUTPUT_FILE":
            fprintf(STDERR, "Chyba při pokusu o otevření zadaného výstupního souboru pro zápis (např. kvůli nedostatečnému oprávnění).\n");
            break;
        case "KEY_CONFLICT":
            fprintf(STDERR, "Došlo ke konfliktu názvu sloupců tabulky vznikajících z atributů nebo textových elementů a sloupců reprezentující odkaz.\n");
    }
    exit($code);
}

/*
FUNKCE PRO VÝPIS NÁPOVĚDY
*/

function printHelp($output) {
    fprintf($output,"NÁPOVĚDA:\n
Využívané parametry:
\t--input=filename - zadaný vstupní soubor ve formátu XML
\t--output=filename - zadaný výstupní soubor
\t--header='hlavička' - na začátek výstupního souboru se vloží zakomentovaná hlavička
\t--etc=n - pro n>=0 určuje maximální počet sloupců vzniklých ze stejnojmenných podelementů
\t-a - nebudou se generovat sloupce z atributů ve vstupním XML souboru
\t-b - pokud element bude obsahovat více podelementů stejného názvu, bude se uvažovat, jakoby zde byl pouze jediný takový, ale s tím že bude uvažován ten s nejvyšším datovým typemdle odstavce Řešení konfliktů (tento parametr nesmí být kombinován s parametrem --etc=n)
");
    exit(0);
}

?>